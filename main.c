#include <mpi.h>
#include "common/utility.h"
#include <assert.h>
#include <string.h>
#include <float.h>

int MPI_Bcast(void *buffer, int count, MPI_Datatype datatype, int root, MPI_Comm comm)
{
    MPI_Barrier(comm);

    int comm_rank = 0; MPI_Comm_rank(comm, &comm_rank);
    int comm_size = 0; MPI_Comm_size(comm, &comm_size);

    if(root == comm_rank) {
        for(int rank = 0; rank < comm_size; ++rank) {
            if(rank == comm_rank) {
                continue;
            }
            MPI_Send(buffer, count, datatype, rank, 0, comm);
        }
    } else {
        MPI_Recv(buffer, count, datatype, root, MPI_ANY_TAG, comm, MPI_STATUS_IGNORE);
    }

    MPI_Barrier(comm);
    return MPI_SUCCESS;
}

int MPI_Gather(const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm)
{
    MPI_Barrier(comm);

    int comm_rank = 0; MPI_Comm_rank(comm, &comm_rank);
    int comm_size = 0; MPI_Comm_size(comm, &comm_size);

    int sendtypesize = 0; MPI_Type_size(sendtype, &sendtypesize);
    int recvtypesize = 0; MPI_Type_size(recvtype, &recvtypesize);
    assert(sendcount * sendtypesize == recvcount * recvtypesize);
    int chunksize = recvcount * recvtypesize;

    if(root == comm_rank) {
        for(int rank = 0; rank < comm_size; ++rank) {
            if(root == rank) {
                memcpy(recvbuf + rank * chunksize, sendbuf, chunksize);
            } else {
                char tempbuf[chunksize];
                MPI_Status status;
                MPI_Recv(tempbuf, recvcount, recvtype, MPI_ANY_SOURCE, MPI_ANY_TAG, comm, &status);
                memcpy(recvbuf + status.MPI_SOURCE * chunksize, tempbuf, chunksize);
            }
        }
    } else {
        MPI_Send(sendbuf, sendcount, sendtype, root, 0, comm);
    }

    MPI_Barrier(comm);
    return MPI_SUCCESS;
}

int MPI_Scatter(const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm)
{
    MPI_Barrier(comm);

    int comm_rank = 0; MPI_Comm_rank(comm, &comm_rank);
    int comm_size = 0; MPI_Comm_size(comm, &comm_size);

    int sendtypesize = 0; MPI_Type_size(sendtype, &sendtypesize);
    int recvtypesize = 0; MPI_Type_size(recvtype, &recvtypesize);
    assert(sendcount * sendtypesize == recvcount * recvtypesize);
    int chunksize = recvcount * recvtypesize;

    if(root == comm_rank) {
        for(int rank = 0; rank < comm_size; ++rank) {
            if(root == rank) {
                memcpy(recvbuf, sendbuf + rank * chunksize, chunksize);
            } else {
                MPI_Send(sendbuf + rank * chunksize, sendcount, sendtype, rank, 0, comm);
            }
        }
    } else {
        MPI_Recv(recvbuf, recvcount, recvtype, root, MPI_ANY_TAG, comm, MPI_STATUS_IGNORE);
    }

    MPI_Barrier(comm);
    return MPI_SUCCESS;
}

int MPI_Reduce(const void *sendbuf, void *recvbuf, int count, MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm)
{
    MPI_Barrier(comm);

    int comm_rank = 0; MPI_Comm_rank(comm, &comm_rank);
    int comm_size = 0; MPI_Comm_size(comm, &comm_size);

    if(MPI_INT != datatype) {
        assert(!"Sorry not implemented (USE MPI_INT)");
    }

    if(root == comm_rank) {
        int data[count * comm_size];
        for(int rank = 0; rank < comm_size; ++rank) {
            if(root == rank) {
                memcpy(data + rank * count, sendbuf, count * sizeof(int));
            } else {
                MPI_Recv(data + rank * count, count, datatype, MPI_ANY_SOURCE, MPI_ANY_TAG, comm, MPI_STATUS_IGNORE);
            }
        }
        int *result = (int*)recvbuf;
        memcpy(result, data, sizeof(int) * count);
        for(int rank = 1; rank < comm_size; ++rank) {
            for(int i = 0; i < count; ++i) {
                switch(op) {
                    case MPI_MAX : {
                        result[i] = max(result[i], data[rank * count + i]);
                        break;
                    }
                    case MPI_MIN : {
                        result[i] = min(result[i], data[rank * count + i]);
                        break;
                    }
                    case MPI_SUM : {
                        result[i] = result[i] + data[rank * count + i];
                        break;
                    }
                    case MPI_PROD: {
                        result[i] = result[i] * data[rank * count + i];
                        break;
                    }
                    case MPI_LAND: {
                        result[i] = !!(result[i]) && !!(data[rank * count + i]);
                        break;
                    }
                    case MPI_LOR : {
                        result[i] = !!(result[i]) || !!(data[rank * count + i]);
                        break;
                    }
                    case MPI_LXOR: {
                        result[i] = !!(result[i]) != !!(data[rank * count + i]);
                        break;
                    }
                    case MPI_BAND: {
                        result[i] = result[i] & data[rank * count + i];
                        break;
                    }
                    case MPI_BOR : {
                        result[i] = result[i] | data[rank * count + i];
                        break;
                    }
                    case MPI_BXOR: {
                        result[i] = result[i] ^ data[rank * count + i];
                        break;
                    }
                    default: {
                        assert(!"MPI_Op not supported for MPI_INT");
                    }
                }
            }
        }
    } else {
        MPI_Send(sendbuf, count, datatype, root, 0, comm);
    }

    MPI_Barrier(comm);
    return MPI_SUCCESS;
}

#ifndef BUF_SIZE
#   define BUF_SIZE 10
#endif

#ifndef TIME_AVG_OF
#   define TIME_AVG_OF 1000
#endif

typedef struct {
    double mean;
    double min;
    double max;
} timestat_t;

timestat_t getstat(double *times, int count)
{
    timestat_t stat = {
        .mean = 0,
        .min = DBL_MAX,
        .max = DBL_MIN
    };

    for(int i = 0; i < count; ++i) {
        stat.mean += times[i];
        stat.max = max(stat.max, times[i]);
        stat.min = min(stat.min, times[i]);
    }
    stat.mean /= count;
    return stat;
}

#define OUTPUT_RESULTS

int main(int argc, char *argv[]) MPI_MAIN_TAG(MPI_ERRORS_ARE_FATAL,
{
    { // MPI_Bcast
        double times[TIME_AVG_OF] = {};

        int array[BUF_SIZE] = {};
        if(0 == mpi_rank) {
            for(int i = 0; i < BUF_SIZE; ++i) {
                array[i] = i;
            }
        }
        
        for(int i = 0; i < TIME_AVG_OF; ++i) {
            MPI_Barrier(MPI_COMM_WORLD);
            times[i] -= MPI_Wtime();
            MPI_Bcast(array, BUF_SIZE, MPI_INT, 0, MPI_COMM_WORLD);
            MPI_Barrier(MPI_COMM_WORLD);
            times[i] += MPI_Wtime();
        }
        if(0 == mpi_rank) {
            timestat_t stat = getstat(times, TIME_AVG_OF);
            printf("MPI_Bcast  : time elapsed (wtick = %lf): mean %lf min %lf max %lf\n", 
                                            MPI_Wtick(), stat.mean, stat.min, stat.max);
        }
#ifdef OUTPUT_RESULTS
        printf("MPI_Bcast(%d): ", mpi_rank);
        for(int i = 0; i < BUF_SIZE; ++i) {
            printf(" %d", array[i]);
        }
        printf("\n");
#endif // OUTPUT_RESULTS
    } // \MPI_Bcast

    { // MPI_Reduce
        double times[TIME_AVG_OF] = {};

        int array[BUF_SIZE] = {};
        for(int i = 0; i < BUF_SIZE; ++i) {
            array[i] = mpi_rank + i;
        }

        int result[BUF_SIZE] = {};
        for(int i = 0; i < TIME_AVG_OF; ++i) {
            MPI_Barrier(MPI_COMM_WORLD);
            times[i] -= MPI_Wtime();
            MPI_Reduce(array, result, BUF_SIZE, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
            MPI_Barrier(MPI_COMM_WORLD);
            times[i] += MPI_Wtime();
        }
        if(0 == mpi_rank) {
            timestat_t stat = getstat(times, TIME_AVG_OF);
            printf("MPI_Reduce : time elapsed (wtick = %lf): mean %lf min %lf max %lf\n", 
                                            MPI_Wtick(), stat.mean, stat.min, stat.max);
#ifdef OUTPUT_RESULTS
            printf("Reduce result {");
            for(int i = 0; i < BUF_SIZE; ++i) {
                printf(" %d", result[i]);
            }
            printf(" }\n");
#endif // OUTPUT_RESULTS
        }
    } // \MPI_Reduce

    { // MPI_Scatter
        double times[TIME_AVG_OF] = {};

        int send_buf[BUF_SIZE * mpi_size];
        if(0 == mpi_rank) {
            for(int i = 0; i < sizeof(send_buf) / sizeof(int); ++i) {
                send_buf[i] = i;
            }
        }
        int recv_buf[BUF_SIZE] = {};

        for(int i = 0; i < TIME_AVG_OF; ++i) {
            MPI_Barrier(MPI_COMM_WORLD);
            times[i] -= MPI_Wtime();
            MPI_Scatter(send_buf, BUF_SIZE, MPI_INT, recv_buf, BUF_SIZE, MPI_INT, 0, MPI_COMM_WORLD);
            MPI_Barrier(MPI_COMM_WORLD);
            times[i] += MPI_Wtime();
        }
        if(0 == mpi_rank) {
            timestat_t stat = getstat(times, TIME_AVG_OF);
            printf("MPI_Scatter: time elapsed (wtick = %lf): mean %lf min %lf max %lf\n", 
                                            MPI_Wtick(), stat.mean, stat.min, stat.max);
        }
#ifdef OUTPUT_RESULTS
        printf("MPI_Scatter(%d): ", mpi_rank);
        for(int i = 0; i < BUF_SIZE; ++i) {
            printf(" %d", recv_buf[i]);
        }
        printf("\n");
#endif // OUTPUT_RESULTS
    } // \MPI_Scatter

    { // MPI_Gather
        double times[TIME_AVG_OF] = {};

        int send_buf[BUF_SIZE] = {};
        for(int i = 0; i < sizeof(send_buf) / sizeof(int); ++i) {
            send_buf[i] = mpi_rank + i;
        }
        int recv_buf[BUF_SIZE * mpi_size];

        for(int i = 0; i < TIME_AVG_OF; ++i) {
            MPI_Barrier(MPI_COMM_WORLD);
            times[i] -= MPI_Wtime();
            MPI_Gather(send_buf, BUF_SIZE, MPI_INT, recv_buf, BUF_SIZE, MPI_INT, 0, MPI_COMM_WORLD);
            MPI_Barrier(MPI_COMM_WORLD);
            times[i] += MPI_Wtime();
        }
        if(0 == mpi_rank) {
            timestat_t stat = getstat(times, TIME_AVG_OF);
            printf("MPI_Gather : time elapsed (wtick = %lf): mean %lf min %lf max %lf\n", 
                                            MPI_Wtick(), stat.mean, stat.min, stat.max);
#ifdef OUTPUT_RESULTS
            printf("MPI_Gather: {");
            for(int i = 0; i < BUF_SIZE * mpi_size; ++i) {
                printf(" %d", recv_buf[i]);
            }
            printf(" }\n");
#endif // OUTPUT_RESULTS
        }
    } // \MPI_Gather
})
